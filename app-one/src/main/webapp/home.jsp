<%@page import="org.wso2.carbon.identity.sso.agent.bean.SSOAgentConfig"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Iterator"%>
<%@page import="org.wso2.carbon.identity.sso.agent.SSOAgentConstants"%>
<%@page import="org.wso2.carbon.identity.sso.agent.bean.LoggedInSessionBean"%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Trang chủ</title>
</head>
<body>
	<h2>Welcome Application One</h2>
	<%
    String claimedId = null;
    String subjectId = null;
    Map<String, List<String>> openIdAttributes = null;
    Map<String, String> saml2SSOAttributes = null;
    if(request.getSession(false) != null &&
            request.getSession(false).getAttribute(SSOAgentConstants.SESSION_BEAN_NAME) == null){
        request.getSession().invalidate();
    %>
	<script type="text/javascript">
    	location.href = "index.jsp";
    </script>
	<% return;} %>
	<%
	SSOAgentConfig ssoAgentConfig = (SSOAgentConfig)getServletContext().getAttribute(SSOAgentConstants.CONFIG_BEAN_NAME);
	LoggedInSessionBean sessionBean = (LoggedInSessionBean)session.getAttribute(SSOAgentConstants.SESSION_BEAN_NAME);
	LoggedInSessionBean.AccessTokenResponseBean accessTokenResponseBean = sessionBean.getSAML2SSO().getAccessTokenResponseBean();
	saml2SSOAttributes = sessionBean.getSAML2SSO().getSubjectAttributes();
	%>
	<p>
		You logged in as
		<%=subjectId = sessionBean.getSAML2SSO().getSubjectId()%>
	</p>

	<h4>User profiles:</h4>
	<table>
	<%
    if(saml2SSOAttributes != null) {
    	for (Map.Entry<String, String> entry:saml2SSOAttributes.entrySet()) {
   	%>
		<tr>
			<td style="padding-right: 15px;"><%=entry.getKey()%>:</td>
			<td><%=entry.getValue()%></td>
		</tr>
	<%	}
    }
    %>
	</table>
	
	<%
    if(subjectId != null && ssoAgentConfig.getSAML2().isSLOEnabled()) {
    %>
    <a href="logout?SAML2.HTTPBinding=HTTP-Redirect">Logout (HTTPRedirect)
    </a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="logout?SAML2.HTTPBinding=HTTP-POST">Logout (HTTP Post)</a><br/>

    <%
    }
    %>
</body>
</html>